(use scss)

(define args (command-line-arguments))
(define eval? #t)

(when (and (pair? args) (string=? "-n" (car args)))
  (set! eval? #f)
  (set! args (cdr args)))

(define scss
  (let ((scss (if (null? args)
                  (read)
                  (with-input-from-file (car args) read))))
    (if eval?
        (eval scss)
        scss)))

(write-css scss)
