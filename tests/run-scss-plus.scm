(test-begin "scss->css, scss+")

(test-scss "foo bar { baz: qux }" '(css+ (foo ((// bar) (baz "qux")))))
(test-scss "foo, bar, baz { qux: quux }" '(css+ ((foo bar baz) (qux "quux"))))

(test-scss "foo { bar: baz; qux: quux } bar { baz: qux }"
           '(css+ (foo (bar "baz") 
                       (qux "quux"))
                  (bar (baz "qux")))) 

(test-scss "foo { bar: baz } foo bar { baz: qux }" 
           '(css+ (foo (bar "baz")
                       ((// bar) (baz "qux")))))

(test-scss "foo { bar: baz } foo bar baz foo { baz: qux }" 
           '(css+ (foo (bar "baz")
                       ((// bar) 
                        ((// baz) 
                         ((// foo) (baz "qux")))))))

(test-scss "foo bar { baz: qux } foo baz { qux: quux }" 
           '(css+ (foo ((// bar) (baz "qux"))
                       ((// baz) (qux "quux")))))

(test-scss "foo bar, foo baz { qux: quux }" 
           '(css+ (foo (((// bar) (// baz)) (qux "quux")))))

(test-scss "foo + bar { baz: qux }"
           '(css+ (foo ((+ bar) (baz "qux")))))

(test-scss "foo + bar baz { baz: qux }"
           '(css+ (foo ((+ (// bar baz)) (baz "qux")))))

(test-scss "foo.bar { baz: qux }" '(css+ (foo ((& .bar) (baz "qux")))))
(test-scss "foo.bar.baz { baz: qux }" '(css+ (foo ((& .bar) ((& .baz) (baz "qux"))))))
(test-scss "foo bar.baz { baz: qux }" '(css+ (foo ((// bar) ((& .baz) (baz "qux"))))))


(test-scss "foo baz baz, foo baz qux, foo.qux baz, foo.qux qux, bar baz baz, bar baz qux, bar.qux baz, bar.qux qux { foo: bar }"
           '(css+ ((foo bar)
                   (((// baz) (& .qux))
                    (((// baz) (// qux))
                     (foo "bar"))))))


(test-scss "#foo bar { qux: quux }"
           '(css+ ((= id foo)
                   ((// bar)
                    (qux quux)))))

(test-scss "a x, a y, b x, b y { foo: bar }"
           '(css+ ((a b) (((// x) (// y)) (foo "bar")))))

(test-scss "foo.bar { baz: qux }"
           '(css+ (foo ((& (= class bar)) (baz "qux")))))

(test-scss "#foo.bar { baz: qux }"
           '(css+ ((= id foo) ((& (= class bar)) (baz "qux")))))

(test-end)
