(cond-expand
  (local
   (load-relative "../scss.scm")
   (import scss))
  (else
   (use scss)))

(use test)

;; (test-begin "reader")
;; (define (->char-list s)
;;   (if (string? s) (string->list s) s))

;; (define (parse m s)
;;   (let ((p (longest m)))
;;     (reverse (caar (p (lambda (s) (if (null? s) (begin (print "error: " s) (list)) s)) `((() ,(->char-list s))))))))


;; (define (test-selector e s)
;;   (test (format "\"~A\" -> '~A" s e) (list 'css (list e '(foo "bar"))) (css->scss (format "~A { foo: bar }" s))))

;; (test-selector 'foo "foo  ")
;; (test-selector ':bar ":bar")
;; (test-selector '.baz ".baz")
;; (test-selector '|foo[bar]| "foo[bar]")
;; (test-selector '|foo[bar=baz]| "foo[bar=baz]")
;; ;(test-selector '(|foo[bar=baz]|) "[bar='baz']")
;; (test-selector '|foo[bar\|=baz]| "foo[bar|=baz]")
;; (test-selector '|foo[bar~=baz]| "foo[bar~=baz]")
;; (test-selector '|foo[bar~="baz qux"]| "foo[bar~=\"baz qux\"]")
;; (test-selector 'foo.bar#baz "foo.bar#baz")
;; (test-selector '.bar:foo ".bar:foo")
;; (test-selector '(// foo.bar#baz qux) "foo.bar#baz qux ")
;; (test-selector '(> foo bar) "foo > bar")
;; (test-selector '(+ foo bar) "foo+bar")
;; (test-selector '(> foo (+ bar (// baz qux))) "foo > bar + baz qux")
;; (test-selector '(foo bar baz) "foo, bar, baz")
;; (test-selector '((// foo .bar) |#baz|) "foo .bar, #baz")
;; (test-selector '((> x (// y z.bla)) baz.qux) "x>y 
;; z.bla,baz.qux  ")

;; (test '(css (foo)) (css->scss "foo { }"))
;; (test '(css (foo (bar "baz"))) (css->scss "foo { bar: baz }"))
;; (test '(css (foo (bar "baz"))) (css->scss "foo { bar: baz; }"))
;; (test '(css (foo (bar "baz") (qux "quux"))) (css->scss "foo { bar: baz; qux: quux }"))
;; (test '(css (foo (bar "baz") (qux "quux"))) (css->scss "foo { bar: baz; qux: quux; }"))
;; (test '(css ((foo bar) (baz "qux"))) (css->scss "foo, bar { baz: qux }"))
;; (test '(css ((// foo bar) (baz "qux") (quux "zack"))) (css->scss "foo bar { baz: qux; quux: zack }"))
;; (test '(css ((foo (// bar baz)) (baz "qux") (quux "zack"))) (css->scss "foo, bar baz { 
;; baz: qux; 
;; quux: zack }"))

;; (test '(css) (css->scss ""))
;; (test '(css (foo (bar "baz")) (qux (foo "bar") (baz "qux"))) (css->scss "foo { bar: baz }
;;  qux { foo: bar; baz: qux; }"))

;; (test '(css (foo (bar "baz")) (((// foo.bar baz) |#qux|) (foo "bar") (baz "qux"))) (css->scss "
;; foo { 
;;   bar: baz;
;; }



;; foo.bar baz, #qux
;;  { 

;; foo: bar;
;; baz: qux

;;  }"))

;; (test '(css (foo (bar "foo bar baz"))) (css->scss "foo { bar: foo bar baz; }"))
;; (test '(css (import "foo") (import "bar")) (css->scss "@import url(foo);  @import url(bar);"))
;; (test '(css (foo (margin "0") (background "url(http://foo/bar.baz?bla=123)"))) (css->scss "foo { margin: 0; background: url(http://foo/bar.baz?bla=123) }"))

(test-end)

(define-syntax test-scss
  (syntax-rules ()
    ((_ expected scss)
     (let ((expected* expected))
       (test (with-output-to-string (cut write scss))
             expected*
             (scss->css scss))))))

(test-begin "scss->css")

;; scss->css
(test-error (scss->css '()))
(test-scss "" '(css))
(test-scss "" '(css (foo)))
(test-scss "foo { bar: baz } bar { baz: qux }" '(css (foo (bar "baz")) (bar (baz "qux"))))
(test-scss "foo, bar { bar: baz; qux: quux }" '(css ((foo bar) (bar "baz") (qux "quux"))))
(test-scss "foo { bar: baz !important }" '(css (foo (! (bar "baz")))))
(test-scss "foo > bar + baz qux { foo: bar }" '(css ((> foo (+ bar (// baz qux))) (foo "bar"))))
(test-scss "@import url(foo); @import url(bar);" '(css (import "foo") (import "bar")))
(test-scss "@import url(http://bla/blubb.css); #rumpel.bla > .first { border: 1px solid gold; font-size: 2em }"
           '(css (import "http://bla/blubb.css")
                 ((> |#rumpel.bla| .first)
                  (border "1px solid gold")
                  (font-size "2em"))))

(test-scss "foo { qux: 123 }"
           '(css ((// foo) (qux 123))))

;; examples from the WebIt! SCSS documentation

(test-scss "body { font-family: sans-serif; font-size: 12pt; background-color: navy } p { color: white } p.ex { color: red } h1 { color: yellow }"
           '(css
             (body
              (font-family "sans-serif")
              (font-size "12pt")
              (background-color "navy"))
             (p (color "white"))
             (p.ex (color "red"))
             (h1 (color "yellow"))))

(test-scss "p { color: white }" '(css (p (color "white"))))
(test-scss ".ex { color: red }" '(css (.ex (color "red"))))
(test-scss ".ex { color: red }" '(css ((= class ex) (color "red"))))

(test-scss "a:visited { color: lime }" '(css (a:visited (color "lime"))))
(test-scss "a:visited { color: lime }" '(css ((= pclass a visited) (color "lime"))))


(test-scss ":visited { color: lime }" '(css (:visited (color "lime"))))
(test-scss ":visited { color: lime }" '(css ((= pclass visited) (color "lime"))))


(test-scss "a.external:visited { color: blue }" '(css (a.external:visited (color "blue"))))
(test-scss "a.external:visited { color: blue }" '(css ((= pclass (= class a external) visited) (color "blue"))))

(test-scss ".external:visited { color: blue }" '(css (.external:visited (color "blue"))))
(test-scss ".external:visited { color: blue }" '(css ((= pclass (= class external) visited) (color "blue"))))

(test-scss "#z98y { letter-spacing: 0.3em }" '(css ((= id z98y) (letter-spacing "0.3em"))))
(test-scss "#z98y { letter-spacing: 0.3em }" '(css (|#z98y| (letter-spacing "0.3em"))))

(test-scss "h1#z98y { letter-spacing: 0.5em }" '(css ((= id h1 z98y) (letter-spacing "0.5em"))))
(test-scss  "h1#z98y { letter-spacing: 0.5em }" '(css ((= id h1 z98y) (letter-spacing "0.5em"))))

(test-scss "h1, h2 b, h2 em { color: red }"
           '(css ((h1 (// h2 b) (// h2 em)) (color "red"))))


(test-scss "p { font-size: 12pt !important; font-style: italic }"
           '(css (p (! (font-size "12pt")) (font-style "italic"))))


(test-end)

(load-relative "run-scss-plus.scm")

(test-exit)
